import { Component } from '@angular/core';
import{GetApiService} from'./get-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title="AppComponent";
  id="112118003";
  skuId="";
  skuName="";
  
constructor(private api:GetApiService)
{

}
ngOnInit()
{
  this.api.apiCall().subscribe((data)=>{
    console.warn("get api data",data)
    this.skuId=data['skuId'];
    this.skuName=data['skuName'];
  })
}
}

